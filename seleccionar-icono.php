<!DOCTYPE html>
<html lang="en">
<?php  ?>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Seleccionar icono - Senthia</title>
  <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.css">
  <link rel="stylesheet" href="assets/fonts/fontAwesome/css/all.css">
</head>
<body>
<?php $json_icons = json_decode(file_get_contents('assets/fonts/fontAwesome/metadata/icons.json'), true);
$icons = [];
$i = 0;
foreach($json_icons as $key => $val){
  if ($val['styles'][0] == 'solid') {
    $base = 'fas fa-';
  }
  else if($val['styles'][0] == 'brands'){
    $base = 'fab fa-';
  }

  $icons[$i] = ['name' => $key, 'style' => $val['styles'][0], 'base' => $base];
  $i++;
}

?>
  <div class="container">
    <div class="row">
      <h2>Por favor selecione un icono</h2>
    </div>
    <div class="row" style="margin-bottom: 20px">
      <div class="col-1 text-center">
        <i class="" id="icon" style="font-size: 20pt; color: #78909C; "> </i>
      </div>
      <div class="col-4">
        <input type="text" disabled="" value="" id="nameIcon" style="width: 300px">
      </div>
      <div class="col-1 text-center">
        <!-- <a href="#" onclick="changeIcon('')" type="button" class="btn btn-danger" title="Borrar selccion"><i class="fas fa-trash"></i></a> -->
        <button class="btn btn-danger" onclick="changeIcon('')"  title="Borrar selccion"><i class="fas fa-trash"></i></button>
      </div>
    </div>
    <div class="row">
      <?php
      /**  
      **/
        foreach($icons as $key => $val){
          echo  '<div class="col-xl-1 col-lg-2 col-md-3 col-sm-4 col-6 " style="margin-bottom: 20px">'.
                  '<div class="row justify-content-center" style="">'.
                    '<a href="#" onclick="changeIcon(\''.$val["base"].$val["name"].'\')">'.
                      '<i class="'.$val["base"].$val["name"].'" style="color: #8f8f8f; font-size: 20pt"></i>'.
                    '<input type="hidden" value="'.$val["base"].$val["name"].'"/>'.
                    '</a>'.
                    '<br>'.
                  '</div>'.
                  '<div>'.
                    '<p class="text-center">'.$val["name"].'</p>'.
                  '</div>'.
                '</div>';
        }
        
      ?>
      <a onclick=" "></a>
    </div>
  </div>
<footer>
    <!-- <script src="assets/bootstrap/js/bootstrap.js"></script> -->
    <script>
    function changeIcon(name){
      document.getElementById("icon").classList = name;
      document.getElementById("nameIcon").value = name;
    }
      document.write(
        '<script src="http://' +
          (location.host || '${1:localhost}').split(':')[0] +
          ':${2:80}/livereload.js?snipver=1"></' +
          'script>'
      );
    </script>
</footer>
</body>
</html>